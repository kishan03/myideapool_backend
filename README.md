# MyIdeaPool Backend

Backend server based on python/django-rest-framework.

1. Clone the repository using `git clone https://kishan03@bitbucket.org/kishan03/myideapool_backend.git`
2. Change directory to cloned directory. `cd <path to cloned directory>`
3. Setup: `./setup.sh`
4. Run migrations using `python manage.py migrate`
5. Start the back end server `python manage.py runserver`
