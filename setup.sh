#!/bin/bash

sudo apt-get update
sudo apt-get -y install python3
sudo apt install -y python3-pip
sudo apt install postgresql postgresql-contrib

pip3 install virtualenv
virtualenv -p python3 ideapoolvenv
source ${PWD}/ideapoolvenv/bin/activate
pip3 install -r requirements.txt
