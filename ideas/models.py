"""Idea model module."""
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from users.models import User


class Idea(models.Model):
    """Idea model."""

    content = models.CharField(max_length=255)
    impact = models.IntegerField(
        validators=[MaxValueValidator(10), MinValueValidator(1)])
    ease = models.IntegerField(
        validators=[MaxValueValidator(10), MinValueValidator(1)])
    confidence = models.IntegerField(
        validators=[MaxValueValidator(10), MinValueValidator(1)])
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __repr__(self):
        """Representation of Idea."""
        return "Idea: {}".format(self.content)
