"""Django admin for Idea module."""
from django.contrib import admin

# Register your models here.
from ideas.models import Idea


class IdeaAdmin(admin.ModelAdmin):
    list_display = ('content', 'impact', 'ease', 'confidence',)


admin.site.register(Idea, IdeaAdmin)
