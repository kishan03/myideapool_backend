"""Serializer module for ideas app."""
from rest_framework import serializers

from .models import Idea


class IdeaPartialSerializer(serializers.ModelSerializer):
    """Idea partial serializer for update/patch request."""

    class Meta:
        """Idea partial serializer Meta."""

        model = Idea
        exclude = ('user',)
        write_only_fields = ('password',)


class IdeaSerializer(IdeaPartialSerializer):
    """Idea model serializer."""

    class Meta:
        """Idea model serializer Meta."""

        model = Idea
        fields = '__all__'
        write_only_fields = ('password',)
