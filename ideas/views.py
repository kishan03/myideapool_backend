from rest_framework import viewsets, status
from rest_framework.response import Response

from .models import Idea
from .serializers import IdeaSerializer, IdeaPartialSerializer


class IdeasViewSet(viewsets.ModelViewSet):
    """Create/Update/Retrieve/Delete Ideas."""

    def get_serializer_class(self):
        if self.action == 'update':
            return IdeaPartialSerializer
        return IdeaSerializer

    def get_queryset(self):
        return Idea.objects.filter(user__id=self.request.user.id)

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data.update({'user': request.user.id})
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)
