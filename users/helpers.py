from libgravatar import Gravatar


def gravatar_url(email, size=40):
    g = Gravatar(email)
    return g.get_image(size=size)
