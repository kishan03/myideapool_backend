from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import resolve

from users.views import UserViewSet
from users.models import User


class UserTestCase(TestCase):
    """docstring for UserTestCase"""

    def setUp(self):
        User.objects.create(name="Kishan", email="test@test.com")
        User.objects.create_superuser(email="superuser@test.com",
                                      name="SuperUser", password="test")

    def test_user_is_created(self):
        user = User.objects.get(name="Kishan")
        self.assertEqual(user.is_superuser, False)
        self.assertEqual(user.is_staff, False)
        self.assertEqual(user.is_admin, False)
        self.assertIsInstance(user, User)

    def test_superuser_is_created(self):
        user = User.objects.get(name="SuperUser")
        self.assertEqual(user.is_superuser, True)
        self.assertEqual(user.is_staff, True)
        self.assertEqual(user.is_admin, True)
        self.assertIsInstance(user, User)

    def test_invalid_user_creation(self):
        with self.assertRaises(ValidationError):
            User.objects.create(name="Kishan")
            User.objects.create(email="test@test.com")

    def test_user_login(self):
        user_login = self.client.login(email="superuser@test.com",
                                       password="test")
        self.assertEqual(user_login, True)

    def test_url_redirects(self):
        url = resolve("/me")
        self.assertEqual(url.func.__name__, UserViewSet.__name__)
